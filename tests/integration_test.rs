use rust_studies_dnd_bot::dnd;

#[test]
fn fetching_spell() {
    // TODO: Make local server instead of hitting web. TestContainers?
    // TODO: Test whole result not just spell.name?
    let spell = dnd::fetch_spell("vicious-mockery").unwrap();
    assert_eq!(spell.name, "Vicious Mockery")
}
