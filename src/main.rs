use clap::{App, Arg, ArgGroup};
use std::env;

mod common;
mod dnd;

fn main() {
    let arguments_matched = App::new(clap::crate_name!())
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about("An yet to be dnd bot, currently fetching from open5e API")
        .arg(
            Arg::with_name("class")
                .long("class")
                .help("Retrieves the basic information of the given class")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("spell")
                .long("spell")
                .help("Searches for a particular spell and retrieve its description")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("spells-from")
                .long("spells-from")
                .help("Retrives all spell names from the given class")
                .takes_value(true),
        )
        .group(
            ArgGroup::with_name("basic_functionality")
                .args(&["class", "spell", "spells-from"])
                .required(true),
        )
        .get_matches();

    // TODO Use Pattern matching
    if let Some(class_name) = arguments_matched.value_of("class") {
        return println!("{}", dnd::fetch_class(class_name).unwrap());
    }

    if let Some(spellname) = arguments_matched.value_of("spell") {
        return println!("{}", dnd::fetch_spell(spellname).unwrap());
    }

    if let Some(class_name) = arguments_matched.value_of("spells-from") {
        let spells = dnd::fetch_all_from_class(class_name).unwrap();
        for spell in &spells {
            println!("{}", spell.name)
        }
        return println!("{} Spells", spells.len());
    }
}
