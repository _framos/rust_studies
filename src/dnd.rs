use crate::common::{fetch, fetch_paginated};
use reqwest::Result;

mod classes;
mod spells;
use classes::Classes;
use spells::Spell;

pub fn fetch_class(search_value: &str) -> reqwest::Result<Classes> {
    let search_value = search_value.to_lowercase();
    let url = format!("https://api.open5e.com/classes/{}", search_value);
    let response = fetch::<Classes>(&url)?;
    Ok(response)
}

pub fn fetch_spell(search_value: &str) -> Result<Spell> {
    let search_value = search_value.to_lowercase();
    let url = format!("https://api.open5e.com/spells/{}", search_value);
    let result = fetch::<Spell>(&url)?;
    Ok(result)
}

pub fn fetch_all_from_class(search_value: &str) -> Result<Vec<Spell>> {
    let url = format!(
        "https://api.open5e.com/spells/?dnd_class__icontains={}",
        search_value
    );

    let response = fetch_paginated::<Spell>(&url)?;
    Ok(response.results)
}
