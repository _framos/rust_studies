use reqwest::blocking::{get, Client};
use reqwest::Result;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct ApiResponse<T> {
    next: Option<String>,
    pub count: u16,
    pub results: Vec<T>,
}

pub fn fetch<T>(url: &str) -> Result<T>
where
    T: for<'de> Deserialize<'de>,
{
    let result = get(url)?.json::<T>()?;
    Ok(result)
}

pub fn fetch_paginated<T>(url: &str) -> Result<ApiResponse<T>>
where
    T: for<'de> Deserialize<'de>,
{
    let client = Client::new();
    let mut response = client.get(url).send()?.json::<ApiResponse<T>>()?;
    let mut values = response.results;

    while response.next.is_some() {
        let next_page_url = &response.next.unwrap();
        response = client.get(next_page_url).send()?.json::<ApiResponse<T>>()?;
        values.append(&mut response.results);
    }

    let result = ApiResponse::<T> {
        count: response.count,
        results: values,
        next: None,
    };

    Ok(result)
}
