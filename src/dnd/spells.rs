use serde::Deserialize;
use std::fmt;

#[derive(Deserialize)]
pub struct Spell {
    pub name: String,
    pub dnd_class: String, // TODO: Convert it to Vector?
    pub desc: String,
    pub level_int: u8,
}

impl fmt::Display for Spell {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            formatter,
            "{}\n\n{}\n\nSpell Level: {}\n\nClasses: {}",
            self.name, self.desc, self.level_int, self.dnd_class
        )
    }
}
