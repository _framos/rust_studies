use std::fmt;

#[derive(serde::Deserialize)]
pub struct Classes {
    name: String,
    desc: String,
    hit_dice: String,
}

impl fmt::Display for Classes {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            fmt,
            "{}\n\n{}\n\nHit Dice: {}",
            self.name, self.desc, self.hit_dice
        )
    }
}
